#!/bin/bash

# Login to ECR
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 863518419513.dkr.ecr.us-east-1.amazonaws.com/smoothhr/djangobackend

# Build the image
docker build -f docker/prod.Dockerfile -t smoothhr/djangobackend .

# Tag the image
docker tag smoothhr/djangobackend:latest 863518419513.dkr.ecr.us-east-1.amazonaws.com/smoothhr/djangobackend:latest

# Push to ECR
docker push 863518419513.dkr.ecr.us-east-1.amazonaws.com/smoothhr/djangobackend:latest


docker pull 863518419513.dkr.ecr.us-east-1.amazonaws.com/smoothhr/djangobackend:latest