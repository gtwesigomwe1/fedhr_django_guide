#!/bin/bash

# Fetch secrets from Parameter Store
aws ssm get-parameter --name "/prod/smoothhr/DATABASE_URL" --with-decryption --query "Parameter.Value" --output text > .env.prod

# Start docker compose
docker compose -f docker-compose.prod.yml up -d