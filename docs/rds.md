**Test connection from ec2 to rds**
1. Networking: Add inbound rule to allow traffic from ec2 instance. The ec2 should also have an outbound rule to allow traffic to the rds instance.

    ![inbound_conns_from_ec2](inbound_conns_from_ec2.png)

    ![inbound_conns_from_ec2_2](inbound_conns_from_ec2_2.png)

    ![outbound_conns_from_ec2](outbound_conns_from_ec2.png)

2. Test connection from ec2 to rds
    - Use `telnet` to test connection to rds instance, or
    - Use `nc` to test connection to rds instance
        nc -zv smoothhrdb.cbqm6wm6wo6s.us-east-1.rds.amazonaws.com 5432
        ![ec2_rds_test_connection](ec2_rds_test_connection.png)


**Connecting to RDS from DBeaver**
1. Ensure RDS is publicly accessible
![rds_publicly_accessible](rds_publicly_accessible.png)

2. Networking and Security
    - Allow inbound connections from the internet on the subnet group.
    - Allow inbound connections from the internet on the security group.
3. Connect with DBeaver.
![rds_dbeaver_connection](rds_dbeaver_connection.png)

****