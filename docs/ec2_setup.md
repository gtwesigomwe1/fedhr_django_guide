
## Install docker

1. Connect to ec2 instance `ssh -i smoothhrbackendkeypair.pem ubuntu@54.234.230.53`. The `smoothhrbackendkeypair.pem` should have already been downloaded after creating the ec2 instance, and should have 0400 permissions.

1. Update the package index and install prerequisites
First, log in to your EC2 instance (or local Ubuntu machine) and run the following commands:
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y

1. Add Docker’s official GPG key
Run the following command to add the Docker GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

1. Set up the Docker stable repository
Add the Docker repository:
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


1. Install Docker Engine and CLI
Update the package index and install Docker:
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io -y

1. Start and enable Docker
Start the Docker service:
sudo systemctl start docker
sudo systemctl enable docker

    Verify Docker is running:
    sudo systemctl status docker

1. Verify Docker installation
Run the following to check the Docker version:
docker --version
Run a test container to confirm Docker is working:
sudo docker run hello-world

1.  (Optional) Allow running Docker without sudo
 To run Docker as a non-root user (like ubuntu), add the user to the docker group:
sudo usermod -aG docker $USER
Then, restart your terminal session or run:
newgrp docker


1. (Optional) Install docker-compose
To install docker-compose, run:
sudo apt install docker-compose-plugin

    Verify the installation:
    docker compose version

## Install awscli

## Install the application.