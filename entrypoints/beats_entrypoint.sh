# In the context of Celery and Python development, "Celery Beat" (often referred to simply as "Beat") is a scheduler for periodic tasks. It is an add-on scheduler for Celery.
# It's like a cron job but integrated with Celery for managing recurring tasks.
# Instead of defining task intervals directly in cron files, the scheduling can be defined within the application code
# or using tools like Django Admin (if using django-celery-beat).
echo "--> Starting beats process"
celery -A fedhr.tasks worker -l info --without-gossip --without-mingle --without-heartbeat