#!/bin/bash

# Celery is an open-source distributed task queue written in Python that allows you to run asynchronous and scheduled tasks in the background. 
# It's commonly used to offload time-consuming tasks like sending emails, processing data, generating reports, and web scraping, 
# so that the main application can respond faster to user requests.
# Celery supports various message brokers (like Redis, RabbitMQ, and Amazon SQS) to handle the message queue and manage task execution.

echo "--> Creating database migrations"
python manage.py makemigrations

echo "--> Running database migrations"
python manage.py migrate

echo "--> Starting web process"
gunicorn config.wsgi:application -b 0.0.0.0:8000