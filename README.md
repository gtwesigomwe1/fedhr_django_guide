# FedHR App

---

## 📢 **FedHR Backend.** 📢

1. Provides API to the FedHR Frontend
---


### Docker compose
Docker compose file is in the root directory.

To run the app, use the following command:

```bash
docker compose up
```

To run the app in production mode, use the following command:

```bash
docker compose -f docker-compose.prod.yml up --build
```

ECR

```bash
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 123456789012.dkr.ecr.us-east-1.amazonaws.com
```
